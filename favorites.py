# Welcome to the second Git exercise.
# We're in a Python file this time, but the rules are
# mostly the same.

# Let's see what's different:
first_question = "What is your favorite color?"
first_question_answer = "Red"

second_question = "What is your favorite food?"
second_question_answer = "Sushi"

third_question = "Who is your favorite fictional character? ex. Mickey Mouse, Bugs Bunny"
third_question_answer = "Vegeta"

fourth_question = "What is your favorite animal?"
fourth_question_answer = "Dog"

fifth_question = "What is your favorite programming language? (Hint: You can always say Python!!)"
fifth_question_answer = "Python"
